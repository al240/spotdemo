import time

from selenium import webdriver
from selenium.webdriver.support.select import Select

driver = webdriver.Chrome(executable_path='/Users/synup/Downloads/chromedriver 3')

driver.get("https://www.goodreads.com/")

driver.find_element_by_link_text("Sign In").click()

driver.find_element_by_xpath("//button[contains(text(),'Sign in with email')]").click()

driver.find_element_by_xpath("//input[@name='email']").send_keys("prameet31o@gmail.com")

driver.find_element_by_name("password").send_keys("password")

driver.find_element_by_xpath("//input[@type='submit']").click()

driver.find_element_by_xpath("//img[@alt='Dismiss']").click()

driver.find_element_by_xpath("//header/div[2]/div[1]/div[2]/form[1]/input[1]").send_keys("beloved")

time.sleep(2)

books = driver.find_elements_by_xpath("//div[@class='gr-bookSearchResults__item']/a/div[1]/div[1]")

print(len(books))

for book in books:
    if book.text == 'Beloved':
        book.click()
        break



driver.find_element_by_xpath("//span[contains(text(),'Want to Read')]").click()

time.sleep(2)

driver.find_element_by_xpath("//a[contains(text(),'My Books')]").click()

driver.find_element_by_xpath("//img[@alt='Remove from my books']").click()

time.sleep(2)

alert = driver.switch_to.alert

alert.accept()

driver.find_element_by_xpath("//img[@alt='PK']").click()


driver.find_element_by_xpath("//a[contains(text(),'Sign out')]").click()

driver.close()


